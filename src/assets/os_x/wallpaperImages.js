import puma from './10-0_10.1.png';
import jaguar from './10-2.png'
import panther from './10-3.png'
import tiger from './10-4.png'
import leopard from './10-5.png'
import snowleopard from './10-6.png'
import lion from './10-7.png'
import mountainlion from './10-8.jpg'
import mavericks from './10-9.jpg'
import yosemite from './10-10.jpg'
import elcapitan from './10-11.jpg'
import sierra from './10-12.jpg'
import highsierra from './10-13.jpg'
import mojaveday from './10-14-Day.jpg'
import mojavenight from './10-14-Night.jpg'


const desktopImages = [
    {
        "version":"OS X Puma",
        image: puma
    },
    {
        "version":"OS X Jaguar",
        image: jaguar
    },
    {
        "version":"OS X Panther",
        image: panther
    },
    {
        "version":"OS X Tiger",
        image: tiger
    },
    {
        "version":"OS X Leopard",
        image: leopard
    },
    {
        "version":"OS X Snow Leopard",
        image: snowleopard
    },
    {
        "version":"OS X Lion",
        image: lion
    },
    {
        "version":"OS X Mountain Lion",
        image: mountainlion
    },
    {
        "version":"OS X Mavericks",
        image: mavericks
    },
    {
        "version":"OS Yosemite",
        image: yosemite
    },
    {
        "version":"OS X El Capitan",
        image: elcapitan
    },
    {
        "version":"OS X Sierra",
        image: sierra
    },
    {
        "version":"OS X High Sierra",
        image: highsierra
    },
    {
        "version":"OS X Mojave - Day",
        image: mojaveday
    },
    {
        "version":"OS X Mojave - Night",
        image: mojavenight
    }
];

export default desktopImages;