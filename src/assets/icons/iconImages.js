import chrome from './chrome-icon.png';
import tictactoe from './ttt-icon.png';

const iconImages = [
    {
        name: 'Chrome',
        image: chrome,
        url: '/'
    },
    {
        name: 'Tic Tac Toe',
        image: tictactoe,
        url: '/ttt'
    }
];

export default iconImages;