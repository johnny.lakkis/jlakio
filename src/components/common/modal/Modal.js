import React from 'react';
import { Link } from 'react-router-dom';
import './Modal.css';

function Modal(props) {
    return(
        <div className="modal" id="modal-one" aria-hidden="true">
          <div className="modal-dialog">
            <div className="modal-header">
              <h2>{props.title}</h2>
                <Link to="/desktop" className="btn-close" aria-hidden="true">×</Link>
            </div>
    
            <div className="modal-body">
              <p>{props.content}</p>
            </div>
            
            <div className="modal-footer">
              <Link to="/desktop" className="btn btn-danger" aria-hidden="true">Quit</Link>
              <div className="btn btn-primary" onClick={props.callback}>{props.button}</div>
            </div>
          </div>
        </div>
    )
}

export default Modal; 