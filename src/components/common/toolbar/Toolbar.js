import React from 'react'; 
import ToolbarButtons from '../../chrome/chrome-components/ToolbarButtons';

function Toolbar(props){ 
    return (
        <div className='toolbar'>
            <div className='container'>
                <ToolbarButtons class={props} />
                    {/* <Navs to={this.props.children.props.children} title={this.state.title}/> */}
                </div>
            </div>
    )
}

export default Toolbar;