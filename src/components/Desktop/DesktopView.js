import React, { Component } from 'react'; 
import './DesktopView.css';

import desktopImages from './../../assets/os_x/wallpaperImages';
import ApplicationIcons from './components/ApplicationIcons';

class DesktopView extends Component{ 
    constructor(props){
        super(props);
            this.state = {
                number: 9,
            } 
        this.changeWallpaper = this.changeWallpaper.bind(this);
    }
    
    changeWallpaper() {
       
        if( this.state.number === 14 ) {
            this.setState({number: 0})
        }else {
            this.setState({number: this.state.number + 1})
        }
    }
    
    render(){ 
        const imageURL = `${desktopImages[this.state.number].image}`

        const desktopWallPaper = {
            backgroundImage: `url(${ imageURL })`,
        }

        return(
            <div className='desktop' style={desktopWallPaper}>
                <i className="fas fa-palette" onClick={this.changeWallpaper}></i>
                    <ApplicationIcons />
                    {this.props.children}
            </div>
        )
    };
}

export default DesktopView;