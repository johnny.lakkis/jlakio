import React, { Component } from 'react';
import iconImages from '../../../assets/icons/iconImages';
import './ApplicationIcons.css';
import { NavLink} from 'react-router-dom'

class ApplicationIcons extends Component{
    constructor(){
        super()
        this.state = {
            showMenu: false
        } 
    }

    menuToggle = () => {
        this.setState({showMenu: !this.state.showMenu})
    }
    
    render(){ 
        const self = this;
        const renderIcons = iconImages.map(function(icon, i){
            return(
                <NavLink key={i} to={icon.url} className="icon" onClick={self.menuToggle}>
                    <img className='icon-image' src={icon.image} alt='application icons' />
                        <div className="icon-name">{icon.name}</div>
                    </NavLink>
            )
        })
        
        return(
            <div className="application-icons">
                <i className="fas fa-bars fa-2x" onClick={this.menuToggle}></i>
                    {this.state.showMenu ? 
                    <div className="collapsed-menu" >
                        <div className="icons">{renderIcons}</div>      
                    </div> : ''
                    }
                <div className="icons">{renderIcons}</div> 
            </div>
        )
    }
}
     
export default ApplicationIcons;