import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';

import ChromeView from './ChromeView';
import About from './content/About';
import Jlakio from './content/jlakio';

class App extends Component {
  render() {
    return (
        <ChromeView>
            <Switch> 
                <Route exact path='/' component={Jlakio} currentPath={this.props.location.pathname}/>   
                <Route path='/about' component={About}  currentPath={this.props.location.pathname}/>
            </Switch>
        </ChromeView>
    );
  }
}

export default App;