import React from 'react';

import './css/About.css';

function About(){
    return(
        <React.Fragment>
            <span className="about-text">
                    <p>I love JavaScript and everything web related. </p>
                
                    <p>I am driven to create aesthetic and intuitive web applications that also utilise a robust, well-structured and light-weight backend. </p>
                                        
                    <p>I try and stay close to the JavaScript community and try to keep up to date with any new and upcoming technologies. </p>
                                        
                    <p>This site is a work in progress. I built it from <strong>scratch</strong> without any UI libraries and frameworks other than React. </p>
                    
                    <p>JavaScript (React/Redux, Node.js), HTML, CSS, Java (Spring Boot), Docker and AWS are the main tools in my arsenal.</p>                 
            </span>           
                </React.Fragment>
    );
}

export default About;


