import React from 'react';
import './css/ChromeSearchbar.css';

function SearchBar(props) { 
    return(
        <div className="searchbar container">
            <div className='browser-buttons'>
                <i className="fas fa-arrow-left"></i>
                <i className="fas fa-arrow-right"></i>
                <i className="fas fa-redo-alt"></i>
            </div>
                <input type="text" 
                    className="search" 
                        placeholder={`https://www.jlak.io${props.location[0].props.currentPath}`} 
                            disabled 
                            />
                    <i className="fas fa-ellipsis-v"></i>
        </div>
    )
}

export default SearchBar;