import React from 'react';
import { Link } from 'react-router-dom';

import './css/ToolbarButtons.css';

    const ToolbarButtons = (props) => { 
        let table = []
        const colors = ['red', 'orange', 'green'];
        const icons = ['fa-times', 'fa-window-minimize', 'fa-expand' ];
        const to = ['/desktop', '', ''];
        const Fragment = React.Fragment;

        const propsClass = !props.class ?  '' : props.class.class;

        for (let i = 0; i < 3; i++) {
          table.push(
            <Fragment key={i}>
                { <Link to={to[i]} >
                    <div className={`button-toolbar ${colors[i]}`}>
                         <i className={`fas ${icons[i]} fa-xs hidden`}></i>
                    </div></Link>
                }
            </Fragment>)
        }
        
        return <div className={`button-group ${propsClass}`}>{table}</div> 
        
      }

      export default ToolbarButtons;