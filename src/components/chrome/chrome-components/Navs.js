import React from 'react';
import { NavLink } from 'react-router-dom';
import favicon from '../../../assets/favicon.png';

export default function Navs(props) {
    return(
        <div className='tabs'>{
            props.to.map(function(item, i){ 
                return (
                        <NavLink key={i} to={item.props.path} exact activeClassName="active-link" id="tab">
                            <img src={favicon} alt='favicon icon' className='favicon-icon' />
                                <span className="text-small">{props.title[i]}</span>
                                    <i className="fas fa-times"></i>
                        </NavLink>
                )
            })
        }</div>
    );
}




