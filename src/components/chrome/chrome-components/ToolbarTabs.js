import React from 'react';

const Tabs = ( { action } ) => { 
    const table = [];
    const tabHeadings = ['jlak.io', 'About Me'];
    const types = ['home', 'chrome/about'];
    const Fragment = React.Fragment;
    
    for (let i = 0; i < 2; i++) {
        table.push(
          <Fragment>
              { 
                  <div className='tab' onClick={() => action(types[i])}>
                      <span className='text-small'>{tabHeadings[i]}</span>
                      <i class="fas fa-times"></i>
                  </div>
              }
          </Fragment>)
      }
      
      return <div className='tabs'>{table}</div> 
      
    }

export default Tabs;

