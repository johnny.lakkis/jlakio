import React from 'react';

function Bookmarks(){
    function renderLinks() {
        const bookmarks = [
                {
                    bookmarkName: 'GitHub - johnnylak',
                    url: 'https://github.com/johnnylak',
                    icon: 'fab fa-github'
                },
                {
                    bookmarkName: 'stack overflow - johnnylak',
                    url: 'https://stackoverflow.com/users/3622780/johnnylak',
                    icon: 'fab fa-stack-overflow'
                },
                {
                    bookmarkName: 'LinkedIn',
                    url: 'https://www.linkedin.com/in/john-lakkis-86625595/',
                    icon: 'fab fa-linkedin-in'
                }    
            ];

        return(
            bookmarks.map(function(item, i){ 
                return(
                    <span key={i} className='bookmark-item'>
                        <a href={item.url} target="_blank" rel='noopener noreferrer'>
                            <i className={item.icon}></i>
                            <span className="text-xs-small m-l-5">{item.bookmarkName}</span>
                        </a>
                    </span>
                )
            })
        );
    }
    return( 
        <div className='bookmarks'>
            <div className='container'>
                {renderLinks()}
            </div>
        </div>
    )
}

export default Bookmarks;


