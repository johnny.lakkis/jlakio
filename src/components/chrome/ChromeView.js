import React, { Component } from 'react';

import './ChromeView.css';
import './chrome-components/css/ToolbarButtons.css';
import './chrome-components/css/Nav.css';

import './chrome-components/css/Bookmarks.css';

import ToolbarButtons from './chrome-components/ToolbarButtons';
import Navs from './chrome-components/Navs';
import SearchBar from './chrome-components/ChromeSearchbar';
import Bookmarks from './chrome-components/Bookmarks';

class ChromeView extends Component{
    constructor(props){
        super(props)
            this.state = { 
                to: ['/', '/chrome/about' ],
                title: ['jlak.io', 'About Me']
            }
    }

    render(){ 
        return(
            <div className='chrome-window'>
                <div className='toolbar'>
                    <div className='container'>
                        <ToolbarButtons />
                            <Navs to={this.props.children.props.children} title={this.state.title}/>
                    </div>
                </div>
                    <SearchBar location={this.props.children.props.children}/>
                        <Bookmarks />
                <div className='content'>
                    <div className='container'>
                        {this.props.children}
                    </div>
                </div>
            </div>
        );
    }
}

export default ChromeView;