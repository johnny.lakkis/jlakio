import React, { Component } from 'react';
import './App.css';
import { HashRouter, Switch, Route } from 'react-router-dom';

import DesktopView from './components/Desktop/DesktopView';
import TicTacToe from './components/TicTacToe/TicTacToe';
import ChromeRoutes from './components/chrome/ChromeRoutes';

class App extends Component {
  render() {
    return (
      <HashRouter>
        <DesktopView>
            <Switch>
              <Route path='/desktop' />
              <Route path='/ttt' component={TicTacToe} />
              <Route path='/' component={ChromeRoutes} />
            </Switch>
        </DesktopView>
      </HashRouter>
    );
  }
}

export default App;
